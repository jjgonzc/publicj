<?php
	session_start();
	
	/*Add libraries and classes needed*/
	require '../vendor/autoload.php';	  
	require '../vendor/alexistm/simple-json-php/includes/json.php'; 
	require '../class/core.php';

	use \Simple\json;

	
	$SizeTable = isset($_GET['SizeTable']) ? $_GET['SizeTable'] : 0;
	$NumQuery = isset($_GET['NumQuery']) ? $_GET['NumQuery'] : 0;
	$Matrix = array(array(),array(),array());
	$MatrixAdded = array();

	$SumT = 0;

	/* Creating the table and filling in zeros */

	for($i=0; $i<=$SizeTable; $i++){
	  for($j=0; $j<=$SizeTable; $j++){
	    for($k=0; $k<=$SizeTable; $k++){
	      $Matrix[$i][$j][$k] = 0;
	    }
	  }
	}
	/* */

	$Core = new Core(); // Initializing Core

	$_SESSION["matrix"] = $Matrix;
	$_SESSION["matrixAdded"] = $MatrixAdded;

	$x;
	$y;
	$z;
	$x0;
	$y0;
	$z0;
	$value1;
	$value2;
	$val;

	$Matrix = $_SESSION["matrix"];
	$MatrixAdded = $_SESSION["matrixAdded"];
	$json = new json();

	$countj =0;

	for ($nq=1; $nq <= $NumQuery ; $nq++) {

		$Query = $_POST["Query".$nq];

		$Query = explode(" ", $Query);

		if ($Query[0] == "QUERY") {
			$x0= $Query[1];
			$y0= $Query[2];
			$z0= $Query[3];
			$x= isset($Query[4]) ? ($Query[4]-1): 0;
			$y= isset($Query[5]) ? ($Query[5]-1): 0;
			$z= isset($Query[6]) ? ($Query[6]-1): 0;

			$Matrix = $_SESSION["matrix"];		
			$SumT = $Core->CalculateSum2($x0,$y0,$z0,$x,$y,$z,$Matrix, $SumT, $MatrixAdded);

			$countj++;
			
			$json->$countj = $SumT;
			
		}

		if ($Query[0] == "UPDATE") {
			
            $x0 = $Query[1];
            $y0 = $Query[2];
            $z0 = $Query[3];
            $x = $Query[1];
            $y = $Query[2];
            $z = $Query[3];

            $value1 = $Core->CalculateSum($x,$y,$z,$Matrix)- $Core->CalculateSum($x0-1,$y,$z,$Matrix) 
                    - $Core->CalculateSum($x,$y0-1,$z,$Matrix) + $Core->CalculateSum($x0-1,$y0-1,$z,$Matrix);
            $value2 = $Core->CalculateSum($x,$y,$z0-1,$Matrix) - $Core->CalculateSum($x0-1,$y,$z0-1,$Matrix)
                    - $Core->CalculateSum($x,$y0-1,$z0-1,$Matrix)  + $Core->CalculateSum($x0-1,$y0-1,$z0-1,$Matrix);

            $Core->update($SizeTable,$x,$y,$z,$Query[4] -($value1 - $value2 ),$Matrix);
		}
	}
	$json->send();


?>